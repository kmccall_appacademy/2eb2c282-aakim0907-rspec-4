class Dictionary
  def initialize
    @dict = {}
  end

  def entries
    @dict
  end

  def keywords
    @dict.keys.sort
  end

  def add(arg)
    arg.is_a?(Hash) ? arg.each {|k,v| @dict[k] = v} : @dict[arg] = nil
  end

  def include?(key)
    @dict.keys.include?(key)
  end

  def find(key)
    finds = Hash.new
    @dict.each {|k,v| finds[k] = @dict[k] if k.include?(key)}
    finds
  end

  def printable
    printing = ""
    self.keywords.each {|k| printing += "[#{k}] \"#{@dict[k]}\"\n"}
    printing.chomp
  end
end
