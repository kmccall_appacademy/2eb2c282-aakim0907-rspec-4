class Book
  def title=(title)
    no = ['the', 'a', 'an', 'and', 'in', 'the', 'of']
    wrds, titlized = title.split, []
    wrds.each_with_index do |wrd, idx|
      no.include?(wrd) && idx != 0 ? titlized << wrd : titlized << wrd.capitalize
    end
    @title = titlized.join(" ")
  end

  def title
    @title
  end
end
