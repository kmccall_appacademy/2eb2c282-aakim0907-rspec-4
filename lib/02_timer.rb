class Timer
  def initialize(seconds=0)
    @seconds = seconds
  end

  def seconds=(seconds)
    @seconds = seconds
  end

  def seconds
    @seconds
  end

  def time_string
    hr, min, sec = "", "", ""
    sec = @seconds % 60
    min = @seconds / 60
    hr = min / 60
    min = min % 60
    "#{padded(hr)}:#{padded(min)}:#{padded(sec)}"
  end

  def padded(int)
    int < 10 ? "0#{int.to_s}" : int.to_s
  end
end
