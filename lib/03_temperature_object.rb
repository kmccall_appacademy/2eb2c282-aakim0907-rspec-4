class Temperature
  def initialize(arg)
    @f = arg[:f]
    @c = arg[:c]
  end

  def in_celsius
    @c or (@f - 32) * 5.fdiv(9)
  end

  def in_fahrenheit
    @f or @c * 9.fdiv(5) + 32
  end

  def self.from_celsius(temp)
    Temperature.new({c:temp})
  end

  def self.from_fahrenheit(temp)
    Temperature.new({f:temp})
  end
end

class Celsius < Temperature
  def initialize(temp)
    super({c:temp})
  end
end

class Fahrenheit < Temperature
  def initialize(temp)
    super({f:temp})
  end
end
